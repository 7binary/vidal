<?php
namespace Vidal\MainBundle\Service;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\EventListener\MergeCollectionListener;
use Symfony\Bridge\Propel1\Form\DataTransformer\ModelToIdTransformer;

class PatchedModelType extends \Sonata\AdminBundle\Form\Type\ModelType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['multiple']) {
            $builder
                ->addEventSubscriber(new MergeCollectionListener($options['model_manager']))
                ->addViewTransformer(new PatchedModelsToArrayTransformer($options['choice_list']), true)
            ;
        } else {
            $builder
                ->addViewTransformer(new ModelToIdTransformer($options['model_manager'], $options['class']), true)
            ;
        }
    }
}