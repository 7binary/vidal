<?php
namespace Vidal\MainBundle\Service;

use Symfony\Component\Form\ChoiceList\LegacyChoiceListAdapter;
use Sonata\AdminBundle\Form\ChoiceList\ModelChoiceList;

class PatchedModelsToArrayTransformer extends \Sonata\AdminBundle\Form\DataTransformer\ModelsToArrayTransformer
{
    public function __construct($choiceList)
    {
        if ($choiceList instanceof LegacyChoiceListAdapter) {
            $this->choiceList = $choiceList->getAdaptedList();
        } else if ($choiceList instanceof ModelChoiceList) {
            $this->choiceList = $choiceList;
        } else {
            throw new \InvalidArgumentException('Argument 1 must be an instance of Sonata\AdminBundle\Form\ChoiceList\ModelChoiceList or Symfony\Component\Form\ChoiceList\LegacyChoiceListAdapter');
        }
    }
}